﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace UI_Imprimantes
{

    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }
        static public List<string> compiledData = new List<string>();
        static public List<string> compiledData2 = new List<string>();
        static public string[,] printerArray = new string[30, 30];
        static public bool IsChartOpen = false;
        static int x;
        //Creation d'un graphique avec l'outils chart de visual studio
        Chart chrt;
        static public string[] row;

        /// <summary>
        /// Affiche les informations de l'imprimantes selectionnée
        /// </summary>
        /// <param name="_x">C'est l'index de l'imprimantes</param>
        private void Show(int _x)
        {
            //Ecrit dans les labels les informations correspondantes se trouvant dans le tableau "compildedData"
            lblModel.Text = compiledData[1+x*15];
            lblPrice.Text = compiledData[2+x*15] +" CHF";
            lblReleasePrice.Text = compiledData[3+x*15] + " CHF";
            lblWeight.Text = compiledData[4 + x * 15 ] + " kg";
            lblDimension.Text = compiledData[5 + x * 15] +" x "+compiledData[6 + x * 15] + " x " + compiledData[7 + x * 15];
            lblPrintSpeed.Text = compiledData[8 + x * 15];
            lblResolution.Text = compiledData[9 + x * 15];
            lblSides.Text = compiledData[10 + x * 15];
            lblPrinterSite.Text = compiledData[11 + x * 15];

            //Dans compiledData nous avons l'index des marques et constructeur
            //On a donc fait un switch en fonction de la foreign Key pour afficher la bonne marque
            switch(compiledData[12 + x * 15])
            {
                case "1":
                    lblBrand.Text = "Brother";
                    lblProvider.Text = "Brother";
                    lblBuilder.Text = "Brother";
                    break;
                case "2":
                    lblBrand.Text = "Canon";
                    lblProvider.Text = "Canon";
                    lblBuilder.Text = "Canon";
                    break;
                case "3":
                    lblBrand.Text = "Epson";
                    lblProvider.Text = "Epson";
                    lblBuilder.Text = "Epson";
                    break;
                case "4":
                    lblBrand.Text = "HP";
                    lblProvider.Text = "HP";
                    lblBuilder.Text = "HP";
                    break;
                case "5":
                    lblBrand.Text = "Xerox";
                    lblProvider.Text = "Xerox";
                    lblBuilder.Text = "Xerox";
                    break;
            }
            
            //Ecrit dans les labels les informations des cartouches se trouvant dans le tableau "compliedData2"
            lblCartridgePrice.Text = compiledData2[3 + x * 5];
            lblCartridgeBrand.Text = compiledData2[1 + x * 5];
            lblCartridgeType.Text = compiledData2[2 + x * 5];

            
            //C'est un switch pour mettre l'image correspondante de l'imprimantes en fonction de l'index de l'imprimantes
            switch (_x+1)
            {
                case 1:
                    pbPrinter.Image = Properties.Resources._1_Brother_MFC_L8690CDW;
                    break;
                case 2:
                    pbPrinter.Image = Properties.Resources._2_Brother_DCP_L2530DW;
                    break;
                case 3:
                    pbPrinter.Image = Properties.Resources._3_Brother_DCP_J774DW;
                    break;
                case 4:
                    pbPrinter.Image = Properties.Resources._4_Brother_MFC_J1300DW;
                    break;
                case 5:
                    pbPrinter.Image = Properties.Resources._5_Brother_MFC_L3710CW;
                    break;
                case 6:
                    pbPrinter.Image = Properties.Resources._6_Brother_MFC_L3770CDW;
                    break;
                case 7:
                    pbPrinter.Image = Properties.Resources._7_Brother_MFC_L2710DW;
                    break;
                case 8:
                    pbPrinter.Image = Properties.Resources._8_Brother_MFC_J6930DW;
                    break;
                case 9:
                    pbPrinter.Image = Properties.Resources._9_Brother_MFC_L2750DW;
                    break;
                case 10:
                    pbPrinter.Image = Properties.Resources._10_Brother_MFC_L2730DW;
                    break;
                case 11:
                    pbPrinter.Image = Properties.Resources._11_Canon_Pixma_TS6251;
                    break;
                case 12:
                    pbPrinter.Image = Properties.Resources._12_Canon_Pixma_TS8150;
                    break;
                case 13:
                    pbPrinter.Image = Properties.Resources._13_Canon_Pixma_TS9550;
                    break;
                case 14:
                    pbPrinter.Image = Properties.Resources._14_Canon_Pixma_G4510;
                    break;
                case 15:
                    pbPrinter.Image = Properties.Resources._15_Canon_Pixma_TS9155;
                    break;
                case 16:
                    pbPrinter.Image = Properties.Resources._16_Canon_Pixma_MG3650S;
                    break;
                case 17:
                    pbPrinter.Image = Properties.Resources._17_Canon_MF643Cdw_i_SENSYS;
                    break;
                case 18:
                    pbPrinter.Image = Properties.Resources._18_Expression_Premium_XP_6105;
                    break;
                case 19:
                    pbPrinter.Image = Properties.Resources._19_EPSON_EcoTank_ET_2720;
                    break;
                case 20:
                    pbPrinter.Image = Properties.Resources._20_EPSON_WorkForce_WF_7710DWF;
                    break;
                case 21:
                    pbPrinter.Image = Properties.Resources._21_Epson_EcoTank_ET_2726;
                    break;
                case 22:
                    pbPrinter.Image = Properties.Resources._22_WorkForce_WF_3720DWF;
                    break;
                case 23:
                    pbPrinter.Image = Properties.Resources._23_HP_Deskjet_2620;
                    break;
                case 24:
                    pbPrinter.Image = Properties.Resources._24_HP_DeskJet_3750;
                    break;
                case 25:
                    pbPrinter.Image = Properties.Resources._25_HP_Smart_Tank_Plus_555;
                    break;
                case 26:
                    pbPrinter.Image = Properties.Resources._26_HP_OfficeJet_Pro_9019_AiO;
                    break;
                case 27:
                    pbPrinter.Image = Properties.Resources._27_HP_ENVY_Photo_7830;
                    break;
                case 28:
                    pbPrinter.Image = Properties.Resources._28_HP_Officejet_5232;
                    break;
                case 29:
                    pbPrinter.Image = Properties.Resources._29_Xerox_VersaLink_C500DN;
                    break;
                case 30:
                    pbPrinter.Image = Properties.Resources._30_Xerox_Phaser_6510N;
                    break;
            }
        }
        
        /// <summary>
        /// Lorsque l'on clique sur une imprimantes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Printer_Click(object sender, EventArgs e)
        {
            #region dataFilling
            #endregion          
            //En fonction de l'imprimante sur laquelle on clique, on affiche les informations voulue
            switch (((Control)sender).Name)
            {

                case "1":
                    x = 0;
                    Show(x);
                    break;
                case "2":
                    x = 1;
                    Show(x);
                    break;
                case "3":
                    x = 2;
                    Show(x);
                    break;
                case "4":
                    x = 3;
                    Show(x);
                    break;
                case "5":
                    x = 4;
                    Show(x);
                    break;
                case "6":
                    x = 5;
                    Show(x);
                    break;
                case "7":
                    x = 6;
                    Show(x);
                    break;
                case "8":
                    x = 7;
                    Show(x);
                    break;
                case "9":
                    x = 8;
                    Show(x);
                    break;
                case "10":
                    x = 9;
                    Show(x);
                    break;
                case "11":
                    x = 10; 
                    Show(x);
                    break;
                case "12":
                    x = 11;
                    Show(x);
                    break;
                case "13":
                    x = 12;
                    Show(x);
                    break;
                case "14":
                    x = 13;
                    Show(x);
                    break;
                case "15":
                    x = 14;
                    Show(x);
                    break;
                case "16":
                    x = 15;
                    Show(x);
                    break;
                case "17":
                    x = 16;
                    Show(x);
                    break;
                case "18":
                    x = 17;
                    Show(x);
                    break;
                case "19":
                    x = 18;
                    Show(x);
                    break;
                case "20":
                    x = 19;
                    Show(x);
                    break;
                case "21":
                    x = 20;
                    Show(x);
                    break;
                case "22":
                    x = 21;
                    Show(x);
                    break;
                case "23":
                    x = 22;
                    Show(x);
                    break;
                case "24":
                    x = 23;
                    Show(x);
                    break;
                case "25":
                    x = 24;
                    Show(x);
                    break;
                case "26":
                    x = 25;
                    Show(x);
                    break;
                case "27":
                    x = 26;
                    Show(x);
                    break;
                case "28":
                    x = 27;
                    Show(x);
                    break;
                case "29":
                    x = 28;
                    Show(x);
                    break;
                case "30":
                    x = 29;
                    Show(x);
                    break;
             
            }
            
            //Si le chart est ouvert et que l'on change d'imprimantes
            if (IsChartOpen == true)
            {
                //On ferme le chart
                IsChartOpen = false;
                chrt.Close();
            }

        }
        

        /// <summary>
        /// 
        /// </summary>
        public void ConnectDataBase()
        {
            string table;
            table = "t_imprimante";
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=root;database = p_042_imprimantes;";
            string query = "use p_042_imprimantes;";

            // Prepare the connection
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                query += "SELECT * FROM " + table + ";";
                commandDatabase = new MySqlCommand(query, databaseConnection);
                commandDatabase.CommandTimeout = 60;
                // Open the database
                databaseConnection.Open();
                // Execute the query
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // As our database, the array will contain : ID 0, FIRST_NAME 1,LAST_NAME 2, ADDRESS 3
                        // Do something with every received database ROW
                        int nbRow = 15;
                        row = new string[nbRow];
                        for (int x = 0; x < nbRow; x++)
                        {
                            row[x] = reader.GetString(x);
                            compiledData.Add(row[x]);

                        }
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Show any error message.
                MessageBox.Show(ex.Message);
            }



        }

        /// <summary>
        /// 
        /// </summary>
        public void ConnectDataBase2()
        {
            string table;
            table = "t_cartouche";
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=root;database = p_042_imprimantes;";
            string query = "use p_042_imprimantes;";

            // Prepare the connection
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                query += "SELECT * FROM " + table + ";";
                commandDatabase = new MySqlCommand(query, databaseConnection);
                commandDatabase.CommandTimeout = 60;
                // Open the database
                databaseConnection.Open();
                // Execute the query
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // As our database, the array will contain : ID 0, FIRST_NAME 1,LAST_NAME 2, ADDRESS 3
                        // Do something with every received database ROW
                        int nbRow = 5;
                        row = new string[nbRow];
                        for (int x = 0; x < nbRow; x++)
                        {
                            row[x] = reader.GetString(x);
                            compiledData2.Add(row[x]);

                        }
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Show any error message.
                MessageBox.Show(ex.Message);
            }



        }

        
        /// <summary>
        /// Lorsque l'on passe la souris sur le bouton quitter, on change sa couleur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbClose_MouseEnter(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = Color.IndianRed;
        }

        /// <summary>
        /// Lorsque la souris quitte le bouton, on le remet en gris
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbClose_MouseLeave(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = Color.LightGray;

        }

        /// <summary>
        /// Si on clique sur le bouton quitter, ça ferme l'applicaton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        
        /// <summary>
        /// Cette méthode affiche les imprimantes, sur le panel situer sur le côté gauche de la form, dans l'ordre donnée par "lstOrdre"
        /// </summary>
        /// <param name="lstOrdre">Liste contenant l'index des imprimantes dans l'ordre voulu</param>
        public void AffichPan(List<int> lstOrdre)
        {
            //On efface tout ce qui se trouve sur le panel
            pnlChooser.Controls.Clear();

            //Constante contenant le nombre d'imprimantes que nous avons
            const int nbPrinter = 30;

            //tableau de picturebox dans lequel on mettra les images desdifférentes imprimantes
            PictureBox[] pb = new PictureBox[nbPrinter];

            //Variables de positionnement des picturebox dans le panel
            int locationX = 20;
            int locationY = 10;


            //Passe en revue tout les index dans la liste
            foreach(int x in lstOrdre)
            {
                //Créer une picturebox
                pb[x] = new PictureBox();
                //Lui donne une position
                pb[x].Location = new Point(locationX, locationY);
                //Renomme la picturebox
                pb[x].Name = x + 1 + "";
                //Change son bordersyle
                pb[x].BorderStyle = BorderStyle.Fixed3D;
                //Definit sa taille à 160x160
                pb[x].Size = new Size(160, 160);
                //Ajoute la picturebox sur le panel
                pnlChooser.Controls.Add(pb[x]);
                //Lui met comme évenement "Printer_Click"
                pb[x].Click += new System.EventHandler(Printer_Click);
                //Change son sizemode
                pb[x].SizeMode = PictureBoxSizeMode.Zoom;
                //Change sa couleur de fond
                pb[x].BackColor = Color.White;
                //Met le curseur en main, si l'on passe sur la picturebox
                pb[x].Cursor = Cursors.Hand;

                //Switch pour fficher l'image sur l picturebox en fonction de l'index se trouvant dans la liste
                switch (x + 1)
                {
                    case 1:
                        pb[x].Image = Properties.Resources._1_Brother_MFC_L8690CDW;
                        break;
                    case 2:
                        pb[x].Image = Properties.Resources._2_Brother_DCP_L2530DW;
                        break;
                    case 3:
                        pb[x].Image = Properties.Resources._3_Brother_DCP_J774DW;
                        break;
                    case 4:
                        pb[x].Image = Properties.Resources._4_Brother_MFC_J1300DW;
                        break;
                    case 5:
                        pb[x].Image = Properties.Resources._5_Brother_MFC_L3710CW;
                        break;
                    case 6:
                        pb[x].Image = Properties.Resources._6_Brother_MFC_L3770CDW;
                        break;
                    case 7:
                        pb[x].Image = Properties.Resources._7_Brother_MFC_L2710DW;
                        break;
                    case 8:
                        pb[x].Image = Properties.Resources._8_Brother_MFC_J6930DW;
                        break;
                    case 9:
                        pb[x].Image = Properties.Resources._9_Brother_MFC_L2750DW;
                        break;
                    case 10:
                        pb[x].Image = Properties.Resources._10_Brother_MFC_L2730DW;
                        break;
                    case 11:
                        pb[x].Image = Properties.Resources._11_Canon_Pixma_TS6251;
                        break;
                    case 12:
                        pb[x].Image = Properties.Resources._12_Canon_Pixma_TS8150;
                        break;
                    case 13:
                        pb[x].Image = Properties.Resources._13_Canon_Pixma_TS9550;
                        break;
                    case 14:
                        pb[x].Image = Properties.Resources._14_Canon_Pixma_G4510;
                        break;
                    case 15:
                        pb[x].Image = Properties.Resources._15_Canon_Pixma_TS9155;
                        break;
                    case 16:
                        pb[x].Image = Properties.Resources._16_Canon_Pixma_MG3650S;
                        break;
                    case 17:
                        pb[x].Image = Properties.Resources._17_Canon_MF643Cdw_i_SENSYS;
                        break;
                    case 18:
                        pb[x].Image = Properties.Resources._18_Expression_Premium_XP_6105;
                        break;
                    case 19:
                        pb[x].Image = Properties.Resources._19_EPSON_EcoTank_ET_2720;
                        break;
                    case 20:
                        pb[x].Image = Properties.Resources._20_EPSON_WorkForce_WF_7710DWF;
                        break;
                    case 21:
                        pb[x].Image = Properties.Resources._21_Epson_EcoTank_ET_2726;
                        break;
                    case 22:
                        pb[x].Image = Properties.Resources._22_WorkForce_WF_3720DWF;
                        break;
                    case 23:
                        pb[x].Image = Properties.Resources._23_HP_Deskjet_2620;
                        break;
                    case 24:
                        pb[x].Image = Properties.Resources._24_HP_DeskJet_3750;
                        break;
                    case 25:
                        pb[x].Image = Properties.Resources._25_HP_Smart_Tank_Plus_555;
                        break;
                    case 26:
                        pb[x].Image = Properties.Resources._26_HP_OfficeJet_Pro_9019_AiO;
                        break;
                    case 27:
                        pb[x].Image = Properties.Resources._27_HP_ENVY_Photo_7830;
                        break;
                    case 28:
                        pb[x].Image = Properties.Resources._28_HP_Officejet_5232;
                        break;
                    case 29:
                        pb[x].Image = Properties.Resources._29_Xerox_VersaLink_C500DN;
                        break;
                    case 30:
                        pb[x].Image = Properties.Resources._30_Xerox_Phaser_6510N;
                        break;
                }

                //Ajoute 180 a la position y, pour que la suivante se mette plus bas
                locationY += 180;
            }            
        }


        /// <summary>
        /// Lorsqu'on lance l'application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            //Créer une liste et lui met un ordre de 0 à 29
            List<int> lstOdreBase = new List<int>();
            for (int x = 0; x < 30; x++)
            {
                lstOdreBase.Add(x);
            }

            //Met le focus sur le panel sur lequel se trouve les imprimantes
            pnlChooser.Focus();

            //Se connecte aux BDs
            ConnectDataBase();
            ConnectDataBase2();

            //Affiche le panel de côter avec l'ordre de base
            AffichPan(lstOdreBase);            

            //Affiche les informations de la première imprimantes
            Show(0);
        }

        /// <summary>
        /// Nous avons récuperer ces trois méthodes sur internet, ces dernière permettent de déplacer la form avec la souris
        /// </summary>
        private bool mouseDown;
        private Point lastLocation;
        private void FrmMain_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }
        private void FrmMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }
        
        private void FrmMain_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }


        /// <summary>
        /// Lorsque on passe la souris sur la panel d'imprimantes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlChooser_MouseEnter(object sender, EventArgs e)
        {
            //Le programme se focus dessus
            pnlChooser.Focus();
        }

        /// <summary>
        /// Lorsque on passe la souris sur la form principale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_MouseEnter(object sender, EventArgs e)
        {
            //Le programme se focus dessus
            pnlChooser.Focus();
        }


        /// <summary>
        /// Lorsque l'on clique sur le bouton de l'évolution du prix
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPriceChart_Click(object sender, EventArgs e)
        {
            //Si aucun chart n'est ouvert
            if (IsChartOpen == false)
            {
                //Créer un chart
                chrt = new Chart();
                //X étant l'imrpimantes séléctionnée
                chrt._x = x;
                //Affichage de la chart
                chrt.Show();
                //Mise à jour de la variable bool
                IsChartOpen = true;
            }
            else
            {
                //Si il est déjà ouvert , alors on le remet en premier plan
                chrt.Focus();
            }
        }
        
        /// <summary>
        /// Lorsque l'on change de manière de trier
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbTri_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Création de la liste avec le nouvelle ordre pour les imprimantes
            List<int> lstImrpimantes = new List<int>();

            //Création d'un dictionnair pour faciliter le tri
            Dictionary<int, double> dicoPrice = new Dictionary<int, double>();
            Dictionary<int, string> dicoName = new Dictionary<int, string>();

            //Switch de la nouvel manière de trier
            switch (cbTri.Text)
            {
                case "Prix [croissant]":

                    for (int x = 0; x < 30; x++)
                    {
                        dicoPrice.Add(x, Convert.ToDouble(compiledData[2 + x * 15]));
                    }
                    
                    foreach (KeyValuePair<int, double> num in dicoPrice.OrderBy(key => key.Value))
                    {
                        //Met dans le dictionnaire l'inde e l'imrpimantes et son prix
                        lstImrpimantes.Add(num.Key);
                    }
                    
                    AffichPan(lstImrpimantes);

                    break;

                case "Prix [décroissant]":
                    //Refait la même chose que pour le trix croisant
                    for (int x = 0; x < 30; x++)
                    {
                        dicoPrice.Add(x, Convert.ToDouble(compiledData[2 + x * 15]));
                    }

                    foreach (KeyValuePair<int, double> num in dicoPrice.OrderBy(key => key.Value))
                    {
                        lstImrpimantes.Add(num.Key);
                    }
                    
                    //Inverse la liste 
                    lstImrpimantes.Reverse();
                    //Affiche la panel avec le nouvelle ordre
                    AffichPan(lstImrpimantes);
                    break;

                case "Modèle  [A - Z]":
                    for (int x = 0; x < 30; x++)
                    {
                        dicoName.Add(x, Convert.ToString(compiledData[1 + x * 15]));
                    }

                    foreach (KeyValuePair<int, string> name in dicoName.OrderBy(key => key.Value))
                    {
                        lstImrpimantes.Add(name.Key);
                    }

                    AffichPan(lstImrpimantes);
                    break;

                case "Modèle  [Z - A]":
                    for (int x = 0; x < 30; x++)
                    {
                        dicoName.Add(x, Convert.ToString(compiledData[1 + x * 15]));
                    }

                    foreach (KeyValuePair<int, string> name in dicoName.OrderBy(key => key.Value))
                    {
                        lstImrpimantes.Add(name.Key);
                    }
                    
                    lstImrpimantes.Reverse();
                    AffichPan(lstImrpimantes);
                    break;
            }
        }
    }


}
