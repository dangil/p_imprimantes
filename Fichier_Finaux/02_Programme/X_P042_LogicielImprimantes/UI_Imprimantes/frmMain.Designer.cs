﻿namespace UI_Imprimantes
{
    partial class frmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlChooser = new System.Windows.Forms.Panel();
            this.pnlShow = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnPriceChart = new System.Windows.Forms.Button();
            this.pbPrinter = new System.Windows.Forms.PictureBox();
            this.lblBrand = new System.Windows.Forms.Label();
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.lblDesignInformation = new System.Windows.Forms.Label();
            this.tlpSecondaryInfo = new System.Windows.Forms.TableLayoutPanel();
            this._lblWeight = new System.Windows.Forms.Label();
            this.lblWeight = new System.Windows.Forms.Label();
            this.lblReleasePrice = new System.Windows.Forms.Label();
            this._lblReleasePrice = new System.Windows.Forms.Label();
            this._lblBuilder = new System.Windows.Forms.Label();
            this._lblPrinterSite = new System.Windows.Forms.Label();
            this.lblPrintSpeed = new System.Windows.Forms.Label();
            this._lblPrintSpeed = new System.Windows.Forms.Label();
            this.lblBuilder = new System.Windows.Forms.Label();
            this._lblProvider = new System.Windows.Forms.Label();
            this.lblProvider = new System.Windows.Forms.Label();
            this.lblPrinterSite = new System.Windows.Forms.Label();
            this.tlpPrimaryInfo = new System.Windows.Forms.TableLayoutPanel();
            this.lblCartridgeBrand = new System.Windows.Forms.Label();
            this._lblCartridgeBrand = new System.Windows.Forms.Label();
            this.lblCartridgeType = new System.Windows.Forms.Label();
            this.lblCartridgePrice = new System.Windows.Forms.Label();
            this._lblCartridgePrice = new System.Windows.Forms.Label();
            this._lblSides = new System.Windows.Forms.Label();
            this._lblCartridgeType = new System.Windows.Forms.Label();
            this.lblSides = new System.Windows.Forms.Label();
            this._lblDimension = new System.Windows.Forms.Label();
            this.lblDimension = new System.Windows.Forms.Label();
            this._lblResolution = new System.Windows.Forms.Label();
            this.lblResolution = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.cbTri = new System.Windows.Forms.ComboBox();
            this.pbClose = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlShow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPrinter)).BeginInit();
            this.pnlInfo.SuspendLayout();
            this.tlpSecondaryInfo.SuspendLayout();
            this.tlpPrimaryInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlChooser
            // 
            this.pnlChooser.AccessibleName = "pnlShow";
            this.pnlChooser.AutoScroll = true;
            this.pnlChooser.AutoScrollMargin = new System.Drawing.Size(0, 20);
            this.pnlChooser.AutoScrollMinSize = new System.Drawing.Size(5, 5);
            this.pnlChooser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(88)))), ((int)(((byte)(94)))));
            this.pnlChooser.Location = new System.Drawing.Point(26, 70);
            this.pnlChooser.Name = "pnlChooser";
            this.pnlChooser.Size = new System.Drawing.Size(221, 585);
            this.pnlChooser.TabIndex = 1;
            this.pnlChooser.MouseEnter += new System.EventHandler(this.pnlChooser_MouseEnter);
            // 
            // pnlShow
            // 
            this.pnlShow.AutoScrollMargin = new System.Drawing.Size(0, 20);
            this.pnlShow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(88)))), ((int)(((byte)(94)))));
            this.pnlShow.Controls.Add(this.panel2);
            this.pnlShow.Controls.Add(this.btnPriceChart);
            this.pnlShow.Controls.Add(this.pbPrinter);
            this.pnlShow.Controls.Add(this.lblBrand);
            this.pnlShow.Controls.Add(this.pnlInfo);
            this.pnlShow.Controls.Add(this.lblModel);
            this.pnlShow.Controls.Add(this.lblPrice);
            this.pnlShow.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlShow.ForeColor = System.Drawing.Color.Black;
            this.pnlShow.Location = new System.Drawing.Point(236, 41);
            this.pnlShow.Name = "pnlShow";
            this.pnlShow.Size = new System.Drawing.Size(745, 614);
            this.pnlShow.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(58)))), ((int)(((byte)(64)))));
            this.panel2.Location = new System.Drawing.Point(0, 220);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(764, 21);
            this.panel2.TabIndex = 1;
            // 
            // btnPriceChart
            // 
            this.btnPriceChart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(58)))), ((int)(((byte)(64)))));
            this.btnPriceChart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPriceChart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPriceChart.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPriceChart.ForeColor = System.Drawing.Color.White;
            this.btnPriceChart.Location = new System.Drawing.Point(594, 145);
            this.btnPriceChart.Name = "btnPriceChart";
            this.btnPriceChart.Size = new System.Drawing.Size(138, 28);
            this.btnPriceChart.TabIndex = 36;
            this.btnPriceChart.Text = "Evolution du prix \r\n\r\n ";
            this.btnPriceChart.UseVisualStyleBackColor = false;
            this.btnPriceChart.Click += new System.EventHandler(this.btnPriceChart_Click);
            // 
            // pbPrinter
            // 
            this.pbPrinter.BackColor = System.Drawing.Color.White;
            this.pbPrinter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbPrinter.Image = global::UI_Imprimantes.Properties.Resources._12_Canon_Pixma_TS8150;
            this.pbPrinter.Location = new System.Drawing.Point(24, 10);
            this.pbPrinter.Name = "pbPrinter";
            this.pbPrinter.Size = new System.Drawing.Size(190, 190);
            this.pbPrinter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbPrinter.TabIndex = 26;
            this.pbPrinter.TabStop = false;
            // 
            // lblBrand
            // 
            this.lblBrand.AutoSize = true;
            this.lblBrand.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(88)))), ((int)(((byte)(94)))));
            this.lblBrand.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrand.ForeColor = System.Drawing.Color.White;
            this.lblBrand.Location = new System.Drawing.Point(221, 57);
            this.lblBrand.Name = "lblBrand";
            this.lblBrand.Size = new System.Drawing.Size(168, 24);
            this.lblBrand.TabIndex = 5;
            this.lblBrand.Text = " BarracudBrand";
            // 
            // pnlInfo
            // 
            this.pnlInfo.AutoScroll = true;
            this.pnlInfo.AutoScrollMinSize = new System.Drawing.Size(5, 5);
            this.pnlInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(88)))), ((int)(((byte)(94)))));
            this.pnlInfo.Controls.Add(this.lblDesignInformation);
            this.pnlInfo.Controls.Add(this.tlpSecondaryInfo);
            this.pnlInfo.Controls.Add(this.tlpPrimaryInfo);
            this.pnlInfo.ForeColor = System.Drawing.Color.Black;
            this.pnlInfo.Location = new System.Drawing.Point(22, 232);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(710, 369);
            this.pnlInfo.TabIndex = 34;
            // 
            // lblDesignInformation
            // 
            this.lblDesignInformation.AutoSize = true;
            this.lblDesignInformation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(88)))), ((int)(((byte)(94)))));
            this.lblDesignInformation.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesignInformation.ForeColor = System.Drawing.Color.White;
            this.lblDesignInformation.Location = new System.Drawing.Point(10, 23);
            this.lblDesignInformation.Name = "lblDesignInformation";
            this.lblDesignInformation.Size = new System.Drawing.Size(199, 36);
            this.lblDesignInformation.TabIndex = 31;
            this.lblDesignInformation.Text = "Informations ";
            // 
            // tlpSecondaryInfo
            // 
            this.tlpSecondaryInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(88)))), ((int)(((byte)(94)))));
            this.tlpSecondaryInfo.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tlpSecondaryInfo.ColumnCount = 2;
            this.tlpSecondaryInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpSecondaryInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpSecondaryInfo.Controls.Add(this._lblWeight, 0, 0);
            this.tlpSecondaryInfo.Controls.Add(this.lblWeight, 1, 0);
            this.tlpSecondaryInfo.Controls.Add(this.lblReleasePrice, 1, 5);
            this.tlpSecondaryInfo.Controls.Add(this._lblReleasePrice, 0, 5);
            this.tlpSecondaryInfo.Controls.Add(this._lblBuilder, 0, 1);
            this.tlpSecondaryInfo.Controls.Add(this._lblPrinterSite, 0, 3);
            this.tlpSecondaryInfo.Controls.Add(this.lblPrintSpeed, 1, 4);
            this.tlpSecondaryInfo.Controls.Add(this._lblPrintSpeed, 0, 4);
            this.tlpSecondaryInfo.Controls.Add(this.lblBuilder, 1, 1);
            this.tlpSecondaryInfo.Controls.Add(this._lblProvider, 0, 2);
            this.tlpSecondaryInfo.Controls.Add(this.lblProvider, 1, 2);
            this.tlpSecondaryInfo.Controls.Add(this.lblPrinterSite, 1, 3);
            this.tlpSecondaryInfo.ForeColor = System.Drawing.Color.Black;
            this.tlpSecondaryInfo.Location = new System.Drawing.Point(16, 227);
            this.tlpSecondaryInfo.Name = "tlpSecondaryInfo";
            this.tlpSecondaryInfo.RowCount = 6;
            this.tlpSecondaryInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tlpSecondaryInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tlpSecondaryInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tlpSecondaryInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tlpSecondaryInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tlpSecondaryInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tlpSecondaryInfo.Size = new System.Drawing.Size(680, 140);
            this.tlpSecondaryInfo.TabIndex = 30;
            // 
            // _lblWeight
            // 
            this._lblWeight.AutoSize = true;
            this._lblWeight.BackColor = System.Drawing.Color.Transparent;
            this._lblWeight.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblWeight.ForeColor = System.Drawing.Color.White;
            this._lblWeight.Location = new System.Drawing.Point(4, 1);
            this._lblWeight.Name = "_lblWeight";
            this._lblWeight.Size = new System.Drawing.Size(47, 18);
            this._lblWeight.TabIndex = 17;
            this._lblWeight.Text = "Poids";
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.BackColor = System.Drawing.Color.Transparent;
            this.lblWeight.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeight.ForeColor = System.Drawing.Color.White;
            this.lblWeight.Location = new System.Drawing.Point(343, 1);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(51, 20);
            this.lblWeight.TabIndex = 2;
            this.lblWeight.Text = " 90 kg";
            // 
            // lblReleasePrice
            // 
            this.lblReleasePrice.AutoSize = true;
            this.lblReleasePrice.BackColor = System.Drawing.Color.Transparent;
            this.lblReleasePrice.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReleasePrice.ForeColor = System.Drawing.Color.White;
            this.lblReleasePrice.Location = new System.Drawing.Point(343, 116);
            this.lblReleasePrice.Name = "lblReleasePrice";
            this.lblReleasePrice.Size = new System.Drawing.Size(57, 20);
            this.lblReleasePrice.TabIndex = 4;
            this.lblReleasePrice.Text = " 249.99";
            // 
            // _lblReleasePrice
            // 
            this._lblReleasePrice.AutoSize = true;
            this._lblReleasePrice.BackColor = System.Drawing.Color.Transparent;
            this._lblReleasePrice.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblReleasePrice.ForeColor = System.Drawing.Color.White;
            this._lblReleasePrice.Location = new System.Drawing.Point(4, 116);
            this._lblReleasePrice.Name = "_lblReleasePrice";
            this._lblReleasePrice.Size = new System.Drawing.Size(110, 18);
            this._lblReleasePrice.TabIndex = 19;
            this._lblReleasePrice.Text = "Prix à la sortie";
            // 
            // _lblBuilder
            // 
            this._lblBuilder.AutoSize = true;
            this._lblBuilder.BackColor = System.Drawing.Color.Transparent;
            this._lblBuilder.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblBuilder.ForeColor = System.Drawing.Color.White;
            this._lblBuilder.Location = new System.Drawing.Point(4, 24);
            this._lblBuilder.Name = "_lblBuilder";
            this._lblBuilder.Size = new System.Drawing.Size(101, 18);
            this._lblBuilder.TabIndex = 21;
            this._lblBuilder.Text = "Constructeur";
            // 
            // _lblPrinterSite
            // 
            this._lblPrinterSite.AutoSize = true;
            this._lblPrinterSite.BackColor = System.Drawing.Color.Transparent;
            this._lblPrinterSite.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblPrinterSite.ForeColor = System.Drawing.Color.White;
            this._lblPrinterSite.Location = new System.Drawing.Point(4, 70);
            this._lblPrinterSite.Name = "_lblPrinterSite";
            this._lblPrinterSite.Size = new System.Drawing.Size(71, 18);
            this._lblPrinterSite.TabIndex = 28;
            this._lblPrinterSite.Text = "Site web";
            // 
            // lblPrintSpeed
            // 
            this.lblPrintSpeed.AutoSize = true;
            this.lblPrintSpeed.BackColor = System.Drawing.Color.Transparent;
            this.lblPrintSpeed.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrintSpeed.ForeColor = System.Drawing.Color.White;
            this.lblPrintSpeed.Location = new System.Drawing.Point(343, 93);
            this.lblPrintSpeed.Name = "lblPrintSpeed";
            this.lblPrintSpeed.Size = new System.Drawing.Size(48, 20);
            this.lblPrintSpeed.TabIndex = 8;
            this.lblPrintSpeed.Text = " 2 P/S";
            // 
            // _lblPrintSpeed
            // 
            this._lblPrintSpeed.AutoSize = true;
            this._lblPrintSpeed.BackColor = System.Drawing.Color.Transparent;
            this._lblPrintSpeed.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblPrintSpeed.ForeColor = System.Drawing.Color.White;
            this._lblPrintSpeed.Location = new System.Drawing.Point(4, 93);
            this._lblPrintSpeed.Name = "_lblPrintSpeed";
            this._lblPrintSpeed.Size = new System.Drawing.Size(155, 18);
            this._lblPrintSpeed.TabIndex = 23;
            this._lblPrintSpeed.Text = "Vitesse d\'impression";
            // 
            // lblBuilder
            // 
            this.lblBuilder.AutoSize = true;
            this.lblBuilder.BackColor = System.Drawing.Color.Transparent;
            this.lblBuilder.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuilder.ForeColor = System.Drawing.Color.White;
            this.lblBuilder.Location = new System.Drawing.Point(343, 24);
            this.lblBuilder.Name = "lblBuilder";
            this.lblBuilder.Size = new System.Drawing.Size(98, 20);
            this.lblBuilder.TabIndex = 6;
            this.lblBuilder.Text = " Barracudax";
            // 
            // _lblProvider
            // 
            this._lblProvider.AutoSize = true;
            this._lblProvider.BackColor = System.Drawing.Color.Transparent;
            this._lblProvider.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblProvider.ForeColor = System.Drawing.Color.White;
            this._lblProvider.Location = new System.Drawing.Point(4, 47);
            this._lblProvider.Name = "_lblProvider";
            this._lblProvider.Size = new System.Drawing.Size(86, 18);
            this._lblProvider.TabIndex = 22;
            this._lblProvider.Text = "Fournisseur";
            // 
            // lblProvider
            // 
            this.lblProvider.AutoSize = true;
            this.lblProvider.BackColor = System.Drawing.Color.Transparent;
            this.lblProvider.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProvider.ForeColor = System.Drawing.Color.White;
            this.lblProvider.Location = new System.Drawing.Point(343, 47);
            this.lblProvider.Name = "lblProvider";
            this.lblProvider.Size = new System.Drawing.Size(98, 20);
            this.lblProvider.TabIndex = 7;
            this.lblProvider.Text = " Barracudax";
            // 
            // lblPrinterSite
            // 
            this.lblPrinterSite.AutoSize = true;
            this.lblPrinterSite.BackColor = System.Drawing.Color.Transparent;
            this.lblPrinterSite.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrinterSite.ForeColor = System.Drawing.Color.White;
            this.lblPrinterSite.Location = new System.Drawing.Point(343, 70);
            this.lblPrinterSite.Name = "lblPrinterSite";
            this.lblPrinterSite.Size = new System.Drawing.Size(125, 20);
            this.lblPrinterSite.TabIndex = 14;
            this.lblPrinterSite.Text = "barracuda.com";
            // 
            // tlpPrimaryInfo
            // 
            this.tlpPrimaryInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(88)))), ((int)(((byte)(94)))));
            this.tlpPrimaryInfo.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tlpPrimaryInfo.ColumnCount = 2;
            this.tlpPrimaryInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpPrimaryInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpPrimaryInfo.Controls.Add(this.lblCartridgeBrand, 1, 5);
            this.tlpPrimaryInfo.Controls.Add(this._lblCartridgeBrand, 0, 5);
            this.tlpPrimaryInfo.Controls.Add(this.lblCartridgeType, 1, 4);
            this.tlpPrimaryInfo.Controls.Add(this.lblCartridgePrice, 1, 3);
            this.tlpPrimaryInfo.Controls.Add(this._lblCartridgePrice, 0, 3);
            this.tlpPrimaryInfo.Controls.Add(this._lblSides, 0, 0);
            this.tlpPrimaryInfo.Controls.Add(this._lblCartridgeType, 0, 4);
            this.tlpPrimaryInfo.Controls.Add(this.lblSides, 1, 0);
            this.tlpPrimaryInfo.Controls.Add(this._lblDimension, 0, 1);
            this.tlpPrimaryInfo.Controls.Add(this.lblDimension, 1, 1);
            this.tlpPrimaryInfo.Controls.Add(this._lblResolution, 0, 2);
            this.tlpPrimaryInfo.Controls.Add(this.lblResolution, 1, 2);
            this.tlpPrimaryInfo.ForeColor = System.Drawing.Color.White;
            this.tlpPrimaryInfo.Location = new System.Drawing.Point(16, 73);
            this.tlpPrimaryInfo.Name = "tlpPrimaryInfo";
            this.tlpPrimaryInfo.RowCount = 6;
            this.tlpPrimaryInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tlpPrimaryInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tlpPrimaryInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tlpPrimaryInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tlpPrimaryInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tlpPrimaryInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tlpPrimaryInfo.Size = new System.Drawing.Size(680, 140);
            this.tlpPrimaryInfo.TabIndex = 29;
            // 
            // lblCartridgeBrand
            // 
            this.lblCartridgeBrand.AutoSize = true;
            this.lblCartridgeBrand.BackColor = System.Drawing.Color.Transparent;
            this.lblCartridgeBrand.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCartridgeBrand.ForeColor = System.Drawing.Color.White;
            this.lblCartridgeBrand.Location = new System.Drawing.Point(343, 116);
            this.lblCartridgeBrand.Name = "lblCartridgeBrand";
            this.lblCartridgeBrand.Size = new System.Drawing.Size(63, 20);
            this.lblCartridgeBrand.TabIndex = 12;
            this.lblCartridgeBrand.Text = " Canon";
            // 
            // _lblCartridgeBrand
            // 
            this._lblCartridgeBrand.AutoSize = true;
            this._lblCartridgeBrand.BackColor = System.Drawing.Color.Transparent;
            this._lblCartridgeBrand.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblCartridgeBrand.ForeColor = System.Drawing.Color.White;
            this._lblCartridgeBrand.Location = new System.Drawing.Point(4, 116);
            this._lblCartridgeBrand.Name = "_lblCartridgeBrand";
            this._lblCartridgeBrand.Size = new System.Drawing.Size(160, 18);
            this._lblCartridgeBrand.TabIndex = 29;
            this._lblCartridgeBrand.Text = "Modèle  Cartouches";
            // 
            // lblCartridgeType
            // 
            this.lblCartridgeType.AutoSize = true;
            this.lblCartridgeType.BackColor = System.Drawing.Color.Transparent;
            this.lblCartridgeType.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCartridgeType.ForeColor = System.Drawing.Color.White;
            this.lblCartridgeType.Location = new System.Drawing.Point(343, 93);
            this.lblCartridgeType.Name = "lblCartridgeType";
            this.lblCartridgeType.Size = new System.Drawing.Size(46, 20);
            this.lblCartridgeType.TabIndex = 13;
            this.lblCartridgeType.Text = " Noir ";
            // 
            // lblCartridgePrice
            // 
            this.lblCartridgePrice.AutoSize = true;
            this.lblCartridgePrice.BackColor = System.Drawing.Color.Transparent;
            this.lblCartridgePrice.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCartridgePrice.ForeColor = System.Drawing.Color.White;
            this.lblCartridgePrice.Location = new System.Drawing.Point(343, 70);
            this.lblCartridgePrice.Name = "lblCartridgePrice";
            this.lblCartridgePrice.Size = new System.Drawing.Size(45, 20);
            this.lblCartridgePrice.TabIndex = 11;
            this.lblCartridgePrice.Text = " 2520";
            // 
            // _lblCartridgePrice
            // 
            this._lblCartridgePrice.AutoSize = true;
            this._lblCartridgePrice.BackColor = System.Drawing.Color.Transparent;
            this._lblCartridgePrice.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblCartridgePrice.ForeColor = System.Drawing.Color.White;
            this._lblCartridgePrice.Location = new System.Drawing.Point(4, 70);
            this._lblCartridgePrice.Name = "_lblCartridgePrice";
            this._lblCartridgePrice.Size = new System.Drawing.Size(123, 18);
            this._lblCartridgePrice.TabIndex = 27;
            this._lblCartridgePrice.Text = "Prix Cartouches";
            // 
            // _lblSides
            // 
            this._lblSides.AutoSize = true;
            this._lblSides.BackColor = System.Drawing.Color.Transparent;
            this._lblSides.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblSides.ForeColor = System.Drawing.Color.White;
            this._lblSides.Location = new System.Drawing.Point(4, 1);
            this._lblSides.Name = "_lblSides";
            this._lblSides.Size = new System.Drawing.Size(99, 18);
            this._lblSides.TabIndex = 25;
            this._lblSides.Text = "Récto-Verso";
            // 
            // _lblCartridgeType
            // 
            this._lblCartridgeType.AutoSize = true;
            this._lblCartridgeType.BackColor = System.Drawing.Color.Transparent;
            this._lblCartridgeType.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblCartridgeType.ForeColor = System.Drawing.Color.White;
            this._lblCartridgeType.Location = new System.Drawing.Point(4, 93);
            this._lblCartridgeType.Name = "_lblCartridgeType";
            this._lblCartridgeType.Size = new System.Drawing.Size(131, 18);
            this._lblCartridgeType.TabIndex = 30;
            this._lblCartridgeType.Text = "Type Cartouches";
            // 
            // lblSides
            // 
            this.lblSides.AutoSize = true;
            this.lblSides.BackColor = System.Drawing.Color.Transparent;
            this.lblSides.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSides.ForeColor = System.Drawing.Color.White;
            this.lblSides.Location = new System.Drawing.Point(343, 1);
            this.lblSides.Name = "lblSides";
            this.lblSides.Size = new System.Drawing.Size(38, 20);
            this.lblSides.TabIndex = 10;
            this.lblSides.Text = " Oui";
            // 
            // _lblDimension
            // 
            this._lblDimension.AutoSize = true;
            this._lblDimension.BackColor = System.Drawing.Color.Transparent;
            this._lblDimension.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblDimension.ForeColor = System.Drawing.Color.White;
            this._lblDimension.Location = new System.Drawing.Point(4, 24);
            this._lblDimension.Name = "_lblDimension";
            this._lblDimension.Size = new System.Drawing.Size(91, 18);
            this._lblDimension.TabIndex = 16;
            this._lblDimension.Text = "Dimensions";
            // 
            // lblDimension
            // 
            this.lblDimension.AutoSize = true;
            this.lblDimension.BackColor = System.Drawing.Color.Transparent;
            this.lblDimension.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDimension.ForeColor = System.Drawing.Color.White;
            this.lblDimension.Location = new System.Drawing.Point(343, 24);
            this.lblDimension.Name = "lblDimension";
            this.lblDimension.Size = new System.Drawing.Size(99, 20);
            this.lblDimension.TabIndex = 1;
            this.lblDimension.Text = " 122x126x247";
            // 
            // _lblResolution
            // 
            this._lblResolution.AutoSize = true;
            this._lblResolution.BackColor = System.Drawing.Color.Transparent;
            this._lblResolution.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblResolution.ForeColor = System.Drawing.Color.White;
            this._lblResolution.Location = new System.Drawing.Point(4, 47);
            this._lblResolution.Name = "_lblResolution";
            this._lblResolution.Size = new System.Drawing.Size(83, 18);
            this._lblResolution.TabIndex = 24;
            this._lblResolution.Text = "Résolution";
            // 
            // lblResolution
            // 
            this.lblResolution.AutoSize = true;
            this.lblResolution.BackColor = System.Drawing.Color.Transparent;
            this.lblResolution.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResolution.ForeColor = System.Drawing.Color.White;
            this.lblResolution.Location = new System.Drawing.Point(343, 47);
            this.lblResolution.Name = "lblResolution";
            this.lblResolution.Size = new System.Drawing.Size(36, 20);
            this.lblResolution.TabIndex = 9;
            this.lblResolution.Text = " 1/3";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(88)))), ((int)(((byte)(94)))));
            this.lblModel.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModel.ForeColor = System.Drawing.Color.White;
            this.lblModel.Location = new System.Drawing.Point(217, 10);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(337, 36);
            this.lblModel.TabIndex = 0;
            this.lblModel.Text = " Barracuda Aggressive";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(88)))), ((int)(((byte)(94)))));
            this.lblPrice.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrice.ForeColor = System.Drawing.Color.White;
            this.lblPrice.Location = new System.Drawing.Point(223, 146);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(117, 22);
            this.lblPrice.TabIndex = 3;
            this.lblPrice.Text = " CHF 267.95";
            // 
            // cbTri
            // 
            this.cbTri.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(88)))), ((int)(((byte)(94)))));
            this.cbTri.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTri.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbTri.ForeColor = System.Drawing.Color.White;
            this.cbTri.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbTri.Items.AddRange(new object[] {
            "Modèle  [A - Z]",
            "Modèle  [Z - A]",
            "Prix [croissant]",
            "Prix [décroissant]"});
            this.cbTri.Location = new System.Drawing.Point(26, 41);
            this.cbTri.Name = "cbTri";
            this.cbTri.Size = new System.Drawing.Size(200, 21);
            this.cbTri.Sorted = true;
            this.cbTri.TabIndex = 38;
            this.cbTri.SelectedIndexChanged += new System.EventHandler(this.cbTri_SelectedIndexChanged);
            // 
            // pbClose
            // 
            this.pbClose.BackColor = System.Drawing.Color.Silver;
            this.pbClose.Location = new System.Drawing.Point(944, 12);
            this.pbClose.Name = "pbClose";
            this.pbClose.Size = new System.Drawing.Size(37, 23);
            this.pbClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbClose.TabIndex = 37;
            this.pbClose.TabStop = false;
            this.pbClose.Click += new System.EventHandler(this.pbClose_Click);
            this.pbClose.MouseEnter += new System.EventHandler(this.pbClose_MouseEnter);
            this.pbClose.MouseLeave += new System.EventHandler(this.pbClose_MouseLeave);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(226, 15);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(19, 643);
            this.panel1.TabIndex = 0;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(58)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1006, 678);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cbTri);
            this.Controls.Add(this.pbClose);
            this.Controls.Add(this.pnlShow);
            this.Controls.Add(this.pnlChooser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Imprimantes";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseDown);
            this.MouseEnter += new System.EventHandler(this.frmMain_MouseEnter);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseUp);
            this.pnlShow.ResumeLayout(false);
            this.pnlShow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPrinter)).EndInit();
            this.pnlInfo.ResumeLayout(false);
            this.pnlInfo.PerformLayout();
            this.tlpSecondaryInfo.ResumeLayout(false);
            this.tlpSecondaryInfo.PerformLayout();
            this.tlpPrimaryInfo.ResumeLayout(false);
            this.tlpPrimaryInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel pnlChooser;
        private System.Windows.Forms.Panel pnlShow;
        private System.Windows.Forms.PictureBox pbPrinter;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblBrand;
        private System.Windows.Forms.Button btnPriceChart;
        private System.Windows.Forms.PictureBox pbClose;
        private System.Windows.Forms.ComboBox cbTri;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Label lblDesignInformation;
        private System.Windows.Forms.TableLayoutPanel tlpSecondaryInfo;
        private System.Windows.Forms.Label _lblWeight;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label lblReleasePrice;
        private System.Windows.Forms.Label _lblReleasePrice;
        private System.Windows.Forms.Label _lblBuilder;
        private System.Windows.Forms.Label _lblPrinterSite;
        private System.Windows.Forms.Label lblPrintSpeed;
        private System.Windows.Forms.Label _lblPrintSpeed;
        private System.Windows.Forms.Label lblBuilder;
        private System.Windows.Forms.Label _lblProvider;
        private System.Windows.Forms.Label lblProvider;
        private System.Windows.Forms.Label lblPrinterSite;
        private System.Windows.Forms.TableLayoutPanel tlpPrimaryInfo;
        private System.Windows.Forms.Label lblCartridgeBrand;
        private System.Windows.Forms.Label _lblCartridgeBrand;
        private System.Windows.Forms.Label lblCartridgeType;
        private System.Windows.Forms.Label lblCartridgePrice;
        private System.Windows.Forms.Label _lblCartridgePrice;
        private System.Windows.Forms.Label _lblSides;
        private System.Windows.Forms.Label _lblCartridgeType;
        private System.Windows.Forms.Label lblSides;
        private System.Windows.Forms.Label _lblDimension;
        private System.Windows.Forms.Label lblDimension;
        private System.Windows.Forms.Label _lblResolution;
        private System.Windows.Forms.Label lblResolution;
        private System.Windows.Forms.Panel panel2;
    }
}

