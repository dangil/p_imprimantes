﻿namespace UI_Imprimantes
{
    partial class frmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlChooser = new System.Windows.Forms.Panel();
            this.pnlShow = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.lblBrand = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._lblWeight = new System.Windows.Forms.Label();
            this.lblWeight = new System.Windows.Forms.Label();
            this.lblReleasePrice = new System.Windows.Forms.Label();
            this._lblReleasePrice = new System.Windows.Forms.Label();
            this._lblBuilder = new System.Windows.Forms.Label();
            this._lblPrinterSite = new System.Windows.Forms.Label();
            this.lblPrintSpeed = new System.Windows.Forms.Label();
            this._lblPrintSpeed = new System.Windows.Forms.Label();
            this.lblBuilder = new System.Windows.Forms.Label();
            this._lblProvider = new System.Windows.Forms.Label();
            this.lblProvider = new System.Windows.Forms.Label();
            this.lblPrinterSite = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblCartridgeBrand = new System.Windows.Forms.Label();
            this._lblCartridgeBrand = new System.Windows.Forms.Label();
            this.lblCartridgeType = new System.Windows.Forms.Label();
            this.lblCartridgePrice = new System.Windows.Forms.Label();
            this._lblCartridgePrice = new System.Windows.Forms.Label();
            this._lblSides = new System.Windows.Forms.Label();
            this._lblCartridgeType = new System.Windows.Forms.Label();
            this.lblSides = new System.Windows.Forms.Label();
            this._lblDimension = new System.Windows.Forms.Label();
            this.lblDimension = new System.Windows.Forms.Label();
            this._lblResolution = new System.Windows.Forms.Label();
            this.lblResolution = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.pbPrinter = new System.Windows.Forms.PictureBox();
            this.pbNb5 = new System.Windows.Forms.PictureBox();
            this.pbNb1 = new System.Windows.Forms.PictureBox();
            this.pbNb4 = new System.Windows.Forms.PictureBox();
            this.pbNb2 = new System.Windows.Forms.PictureBox();
            this.pbNb3 = new System.Windows.Forms.PictureBox();
            this.pbClose = new System.Windows.Forms.PictureBox();
            this.pnlChooser.SuspendLayout();
            this.pnlShow.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPrinter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNb5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNb4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNb2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNb3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlChooser
            // 
            this.pnlChooser.AccessibleName = "pnlShow";
            this.pnlChooser.AutoScroll = true;
            this.pnlChooser.BackColor = System.Drawing.Color.White;
            this.pnlChooser.Controls.Add(this.pbNb5);
            this.pnlChooser.Controls.Add(this.pbNb1);
            this.pnlChooser.Controls.Add(this.pbNb4);
            this.pnlChooser.Controls.Add(this.pbNb2);
            this.pnlChooser.Controls.Add(this.pbNb3);
            this.pnlChooser.Location = new System.Drawing.Point(10, 41);
            this.pnlChooser.Name = "pnlChooser";
            this.pnlChooser.Size = new System.Drawing.Size(201, 614);
            this.pnlChooser.TabIndex = 1;
            // 
            // pnlShow
            // 
            this.pnlShow.AutoScroll = true;
            this.pnlShow.AutoScrollMinSize = new System.Drawing.Size(5, 5);
            this.pnlShow.BackColor = System.Drawing.Color.White;
            this.pnlShow.Controls.Add(this.button1);
            this.pnlShow.Controls.Add(this.pbPrinter);
            this.pnlShow.Controls.Add(this.lblBrand);
            this.pnlShow.Controls.Add(this.panel3);
            this.pnlShow.Controls.Add(this.lblModel);
            this.pnlShow.Controls.Add(this.lblPrice);
            this.pnlShow.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlShow.ForeColor = System.Drawing.Color.Black;
            this.pnlShow.Location = new System.Drawing.Point(222, 41);
            this.pnlShow.Name = "pnlShow";
            this.pnlShow.Size = new System.Drawing.Size(745, 614);
            this.pnlShow.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(589, 148);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 23);
            this.button1.TabIndex = 36;
            this.button1.Text = "Evolution du prix ";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // lblBrand
            // 
            this.lblBrand.AutoSize = true;
            this.lblBrand.BackColor = System.Drawing.Color.White;
            this.lblBrand.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrand.ForeColor = System.Drawing.Color.Black;
            this.lblBrand.Location = new System.Drawing.Point(216, 60);
            this.lblBrand.Name = "lblBrand";
            this.lblBrand.Size = new System.Drawing.Size(168, 24);
            this.lblBrand.TabIndex = 5;
            this.lblBrand.Text = " BarracudBrand";
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.AutoScrollMinSize = new System.Drawing.Size(5, 5);
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.tableLayoutPanel2);
            this.panel3.Controls.Add(this.tableLayoutPanel1);
            this.panel3.ForeColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(17, 206);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(710, 390);
            this.panel3.TabIndex = 34;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(3, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 36);
            this.label1.TabIndex = 31;
            this.label1.Text = "Informations ";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this._lblWeight, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblWeight, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblReleasePrice, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this._lblReleasePrice, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this._lblBuilder, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this._lblPrinterSite, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblPrintSpeed, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this._lblPrintSpeed, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.lblBuilder, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this._lblProvider, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblProvider, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblPrinterSite, 1, 3);
            this.tableLayoutPanel2.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(14, 235);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(680, 140);
            this.tableLayoutPanel2.TabIndex = 30;
            // 
            // _lblWeight
            // 
            this._lblWeight.AutoSize = true;
            this._lblWeight.BackColor = System.Drawing.Color.White;
            this._lblWeight.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblWeight.ForeColor = System.Drawing.Color.Black;
            this._lblWeight.Location = new System.Drawing.Point(5, 2);
            this._lblWeight.Name = "_lblWeight";
            this._lblWeight.Size = new System.Drawing.Size(47, 18);
            this._lblWeight.TabIndex = 17;
            this._lblWeight.Text = "Poids";
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.BackColor = System.Drawing.Color.White;
            this.lblWeight.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeight.ForeColor = System.Drawing.Color.Black;
            this.lblWeight.Location = new System.Drawing.Point(344, 2);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(51, 20);
            this.lblWeight.TabIndex = 2;
            this.lblWeight.Text = " 90 kg";
            // 
            // lblReleasePrice
            // 
            this.lblReleasePrice.AutoSize = true;
            this.lblReleasePrice.BackColor = System.Drawing.Color.White;
            this.lblReleasePrice.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReleasePrice.ForeColor = System.Drawing.Color.Black;
            this.lblReleasePrice.Location = new System.Drawing.Point(344, 117);
            this.lblReleasePrice.Name = "lblReleasePrice";
            this.lblReleasePrice.Size = new System.Drawing.Size(57, 20);
            this.lblReleasePrice.TabIndex = 4;
            this.lblReleasePrice.Text = " 249.99";
            // 
            // _lblReleasePrice
            // 
            this._lblReleasePrice.AutoSize = true;
            this._lblReleasePrice.BackColor = System.Drawing.Color.White;
            this._lblReleasePrice.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblReleasePrice.ForeColor = System.Drawing.Color.Black;
            this._lblReleasePrice.Location = new System.Drawing.Point(5, 117);
            this._lblReleasePrice.Name = "_lblReleasePrice";
            this._lblReleasePrice.Size = new System.Drawing.Size(110, 18);
            this._lblReleasePrice.TabIndex = 19;
            this._lblReleasePrice.Text = "Prix à la sortie";
            // 
            // _lblBuilder
            // 
            this._lblBuilder.AutoSize = true;
            this._lblBuilder.BackColor = System.Drawing.Color.White;
            this._lblBuilder.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblBuilder.ForeColor = System.Drawing.Color.Black;
            this._lblBuilder.Location = new System.Drawing.Point(5, 25);
            this._lblBuilder.Name = "_lblBuilder";
            this._lblBuilder.Size = new System.Drawing.Size(101, 18);
            this._lblBuilder.TabIndex = 21;
            this._lblBuilder.Text = "Constructeur";
            // 
            // _lblPrinterSite
            // 
            this._lblPrinterSite.AutoSize = true;
            this._lblPrinterSite.BackColor = System.Drawing.Color.White;
            this._lblPrinterSite.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblPrinterSite.ForeColor = System.Drawing.Color.Black;
            this._lblPrinterSite.Location = new System.Drawing.Point(5, 71);
            this._lblPrinterSite.Name = "_lblPrinterSite";
            this._lblPrinterSite.Size = new System.Drawing.Size(71, 18);
            this._lblPrinterSite.TabIndex = 28;
            this._lblPrinterSite.Text = "Site web";
            // 
            // lblPrintSpeed
            // 
            this.lblPrintSpeed.AutoSize = true;
            this.lblPrintSpeed.BackColor = System.Drawing.Color.White;
            this.lblPrintSpeed.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrintSpeed.ForeColor = System.Drawing.Color.Black;
            this.lblPrintSpeed.Location = new System.Drawing.Point(344, 94);
            this.lblPrintSpeed.Name = "lblPrintSpeed";
            this.lblPrintSpeed.Size = new System.Drawing.Size(48, 20);
            this.lblPrintSpeed.TabIndex = 8;
            this.lblPrintSpeed.Text = " 2 P/S";
            // 
            // _lblPrintSpeed
            // 
            this._lblPrintSpeed.AutoSize = true;
            this._lblPrintSpeed.BackColor = System.Drawing.Color.White;
            this._lblPrintSpeed.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblPrintSpeed.ForeColor = System.Drawing.Color.Black;
            this._lblPrintSpeed.Location = new System.Drawing.Point(5, 94);
            this._lblPrintSpeed.Name = "_lblPrintSpeed";
            this._lblPrintSpeed.Size = new System.Drawing.Size(155, 18);
            this._lblPrintSpeed.TabIndex = 23;
            this._lblPrintSpeed.Text = "Vitesse d\'impression";
            // 
            // lblBuilder
            // 
            this.lblBuilder.AutoSize = true;
            this.lblBuilder.BackColor = System.Drawing.Color.White;
            this.lblBuilder.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuilder.ForeColor = System.Drawing.Color.Black;
            this.lblBuilder.Location = new System.Drawing.Point(344, 25);
            this.lblBuilder.Name = "lblBuilder";
            this.lblBuilder.Size = new System.Drawing.Size(98, 20);
            this.lblBuilder.TabIndex = 6;
            this.lblBuilder.Text = " Barracudax";
            // 
            // _lblProvider
            // 
            this._lblProvider.AutoSize = true;
            this._lblProvider.BackColor = System.Drawing.Color.White;
            this._lblProvider.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblProvider.ForeColor = System.Drawing.Color.Black;
            this._lblProvider.Location = new System.Drawing.Point(5, 48);
            this._lblProvider.Name = "_lblProvider";
            this._lblProvider.Size = new System.Drawing.Size(86, 18);
            this._lblProvider.TabIndex = 22;
            this._lblProvider.Text = "Fournisseur";
            // 
            // lblProvider
            // 
            this.lblProvider.AutoSize = true;
            this.lblProvider.BackColor = System.Drawing.Color.White;
            this.lblProvider.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProvider.ForeColor = System.Drawing.Color.Black;
            this.lblProvider.Location = new System.Drawing.Point(344, 48);
            this.lblProvider.Name = "lblProvider";
            this.lblProvider.Size = new System.Drawing.Size(98, 20);
            this.lblProvider.TabIndex = 7;
            this.lblProvider.Text = " Barracudax";
            // 
            // lblPrinterSite
            // 
            this.lblPrinterSite.AutoSize = true;
            this.lblPrinterSite.BackColor = System.Drawing.Color.White;
            this.lblPrinterSite.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrinterSite.ForeColor = System.Drawing.Color.Black;
            this.lblPrinterSite.Location = new System.Drawing.Point(344, 71);
            this.lblPrinterSite.Name = "lblPrinterSite";
            this.lblPrinterSite.Size = new System.Drawing.Size(125, 20);
            this.lblPrinterSite.TabIndex = 14;
            this.lblPrinterSite.Text = "barracuda.com";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetPartial;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lblCartridgeBrand, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this._lblCartridgeBrand, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblCartridgeType, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblCartridgePrice, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this._lblCartridgePrice, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this._lblSides, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._lblCartridgeType, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblSides, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this._lblDimension, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDimension, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this._lblResolution, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblResolution, 1, 2);
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(14, 69);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(680, 140);
            this.tableLayoutPanel1.TabIndex = 29;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // lblCartridgeBrand
            // 
            this.lblCartridgeBrand.AutoSize = true;
            this.lblCartridgeBrand.BackColor = System.Drawing.Color.White;
            this.lblCartridgeBrand.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCartridgeBrand.ForeColor = System.Drawing.Color.Black;
            this.lblCartridgeBrand.Location = new System.Drawing.Point(344, 113);
            this.lblCartridgeBrand.Name = "lblCartridgeBrand";
            this.lblCartridgeBrand.Size = new System.Drawing.Size(63, 20);
            this.lblCartridgeBrand.TabIndex = 12;
            this.lblCartridgeBrand.Text = " Canon";
            // 
            // _lblCartridgeBrand
            // 
            this._lblCartridgeBrand.AutoSize = true;
            this._lblCartridgeBrand.BackColor = System.Drawing.Color.White;
            this._lblCartridgeBrand.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblCartridgeBrand.ForeColor = System.Drawing.Color.Black;
            this._lblCartridgeBrand.Location = new System.Drawing.Point(6, 113);
            this._lblCartridgeBrand.Name = "_lblCartridgeBrand";
            this._lblCartridgeBrand.Size = new System.Drawing.Size(159, 18);
            this._lblCartridgeBrand.TabIndex = 29;
            this._lblCartridgeBrand.Text = "Marque  Cartouches";
            // 
            // lblCartridgeType
            // 
            this.lblCartridgeType.AutoSize = true;
            this.lblCartridgeType.BackColor = System.Drawing.Color.White;
            this.lblCartridgeType.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCartridgeType.ForeColor = System.Drawing.Color.Black;
            this.lblCartridgeType.Location = new System.Drawing.Point(344, 91);
            this.lblCartridgeType.Name = "lblCartridgeType";
            this.lblCartridgeType.Size = new System.Drawing.Size(46, 19);
            this.lblCartridgeType.TabIndex = 13;
            this.lblCartridgeType.Text = " Noir ";
            // 
            // lblCartridgePrice
            // 
            this.lblCartridgePrice.AutoSize = true;
            this.lblCartridgePrice.BackColor = System.Drawing.Color.White;
            this.lblCartridgePrice.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCartridgePrice.ForeColor = System.Drawing.Color.Black;
            this.lblCartridgePrice.Location = new System.Drawing.Point(344, 69);
            this.lblCartridgePrice.Name = "lblCartridgePrice";
            this.lblCartridgePrice.Size = new System.Drawing.Size(45, 19);
            this.lblCartridgePrice.TabIndex = 11;
            this.lblCartridgePrice.Text = " 2520";
            // 
            // _lblCartridgePrice
            // 
            this._lblCartridgePrice.AutoSize = true;
            this._lblCartridgePrice.BackColor = System.Drawing.Color.Transparent;
            this._lblCartridgePrice.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblCartridgePrice.ForeColor = System.Drawing.Color.Black;
            this._lblCartridgePrice.Location = new System.Drawing.Point(6, 69);
            this._lblCartridgePrice.Name = "_lblCartridgePrice";
            this._lblCartridgePrice.Size = new System.Drawing.Size(123, 18);
            this._lblCartridgePrice.TabIndex = 27;
            this._lblCartridgePrice.Text = "Prix Cartouches";
            // 
            // _lblSides
            // 
            this._lblSides.AutoSize = true;
            this._lblSides.BackColor = System.Drawing.Color.White;
            this._lblSides.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblSides.ForeColor = System.Drawing.Color.Black;
            this._lblSides.Location = new System.Drawing.Point(6, 3);
            this._lblSides.Name = "_lblSides";
            this._lblSides.Size = new System.Drawing.Size(99, 18);
            this._lblSides.TabIndex = 25;
            this._lblSides.Text = "Récto-Verso";
            // 
            // _lblCartridgeType
            // 
            this._lblCartridgeType.AutoSize = true;
            this._lblCartridgeType.BackColor = System.Drawing.Color.White;
            this._lblCartridgeType.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblCartridgeType.ForeColor = System.Drawing.Color.Black;
            this._lblCartridgeType.Location = new System.Drawing.Point(6, 91);
            this._lblCartridgeType.Name = "_lblCartridgeType";
            this._lblCartridgeType.Size = new System.Drawing.Size(131, 18);
            this._lblCartridgeType.TabIndex = 30;
            this._lblCartridgeType.Text = "Type Cartouches";
            // 
            // lblSides
            // 
            this.lblSides.AutoSize = true;
            this.lblSides.BackColor = System.Drawing.Color.White;
            this.lblSides.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSides.ForeColor = System.Drawing.Color.Black;
            this.lblSides.Location = new System.Drawing.Point(344, 3);
            this.lblSides.Name = "lblSides";
            this.lblSides.Size = new System.Drawing.Size(38, 19);
            this.lblSides.TabIndex = 10;
            this.lblSides.Text = " Oui";
            // 
            // _lblDimension
            // 
            this._lblDimension.AutoSize = true;
            this._lblDimension.BackColor = System.Drawing.Color.White;
            this._lblDimension.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblDimension.ForeColor = System.Drawing.Color.Black;
            this._lblDimension.Location = new System.Drawing.Point(6, 25);
            this._lblDimension.Name = "_lblDimension";
            this._lblDimension.Size = new System.Drawing.Size(91, 18);
            this._lblDimension.TabIndex = 16;
            this._lblDimension.Text = "Dimensions";
            // 
            // lblDimension
            // 
            this.lblDimension.AutoSize = true;
            this.lblDimension.BackColor = System.Drawing.Color.White;
            this.lblDimension.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDimension.ForeColor = System.Drawing.Color.Black;
            this.lblDimension.Location = new System.Drawing.Point(344, 25);
            this.lblDimension.Name = "lblDimension";
            this.lblDimension.Size = new System.Drawing.Size(99, 19);
            this.lblDimension.TabIndex = 1;
            this.lblDimension.Text = " 122x126x247";
            // 
            // _lblResolution
            // 
            this._lblResolution.AutoSize = true;
            this._lblResolution.BackColor = System.Drawing.Color.White;
            this._lblResolution.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblResolution.ForeColor = System.Drawing.Color.Black;
            this._lblResolution.Location = new System.Drawing.Point(6, 47);
            this._lblResolution.Name = "_lblResolution";
            this._lblResolution.Size = new System.Drawing.Size(83, 18);
            this._lblResolution.TabIndex = 24;
            this._lblResolution.Text = "Résolution";
            // 
            // lblResolution
            // 
            this.lblResolution.AutoSize = true;
            this.lblResolution.BackColor = System.Drawing.Color.White;
            this.lblResolution.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResolution.ForeColor = System.Drawing.Color.Black;
            this.lblResolution.Location = new System.Drawing.Point(344, 47);
            this.lblResolution.Name = "lblResolution";
            this.lblResolution.Size = new System.Drawing.Size(36, 19);
            this.lblResolution.TabIndex = 9;
            this.lblResolution.Text = " 1/3";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.BackColor = System.Drawing.Color.White;
            this.lblModel.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModel.ForeColor = System.Drawing.Color.Black;
            this.lblModel.Location = new System.Drawing.Point(210, 10);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(337, 36);
            this.lblModel.TabIndex = 0;
            this.lblModel.Text = " Barracuda Aggressive";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.BackColor = System.Drawing.Color.White;
            this.lblPrice.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrice.ForeColor = System.Drawing.Color.Black;
            this.lblPrice.Location = new System.Drawing.Point(216, 146);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(117, 22);
            this.lblPrice.TabIndex = 3;
            this.lblPrice.Text = " CHF 267.95";
            // 
            // pbPrinter
            // 
            this.pbPrinter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbPrinter.Image = global::UI_Imprimantes.Properties.Resources._12_Canon_Pixma_TS8150;
            this.pbPrinter.Location = new System.Drawing.Point(17, 10);
            this.pbPrinter.Name = "pbPrinter";
            this.pbPrinter.Size = new System.Drawing.Size(190, 190);
            this.pbPrinter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbPrinter.TabIndex = 26;
            this.pbPrinter.TabStop = false;
            // 
            // pbNb5
            // 
            this.pbNb5.BackColor = System.Drawing.Color.White;
            this.pbNb5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbNb5.Location = new System.Drawing.Point(18, 634);
            this.pbNb5.Name = "pbNb5";
            this.pbNb5.Size = new System.Drawing.Size(150, 150);
            this.pbNb5.TabIndex = 9;
            this.pbNb5.TabStop = false;
            this.pbNb5.Tag = "5";
            this.pbNb5.Click += new System.EventHandler(this.pbNb1_Click);
            // 
            // pbNb1
            // 
            this.pbNb1.BackColor = System.Drawing.Color.White;
            this.pbNb1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbNb1.Location = new System.Drawing.Point(18, 10);
            this.pbNb1.Name = "pbNb1";
            this.pbNb1.Size = new System.Drawing.Size(150, 150);
            this.pbNb1.TabIndex = 5;
            this.pbNb1.TabStop = false;
            this.pbNb1.Tag = "1";
            this.pbNb1.Click += new System.EventHandler(this.pbNb1_Click);
            // 
            // pbNb4
            // 
            this.pbNb4.BackColor = System.Drawing.Color.White;
            this.pbNb4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbNb4.Location = new System.Drawing.Point(18, 478);
            this.pbNb4.Name = "pbNb4";
            this.pbNb4.Size = new System.Drawing.Size(150, 150);
            this.pbNb4.TabIndex = 8;
            this.pbNb4.TabStop = false;
            this.pbNb4.Tag = "4";
            this.pbNb4.Click += new System.EventHandler(this.pbNb1_Click);
            // 
            // pbNb2
            // 
            this.pbNb2.BackColor = System.Drawing.Color.White;
            this.pbNb2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbNb2.Location = new System.Drawing.Point(18, 166);
            this.pbNb2.Name = "pbNb2";
            this.pbNb2.Size = new System.Drawing.Size(150, 150);
            this.pbNb2.TabIndex = 6;
            this.pbNb2.TabStop = false;
            this.pbNb2.Tag = "2";
            this.pbNb2.Click += new System.EventHandler(this.pbNb1_Click);
            // 
            // pbNb3
            // 
            this.pbNb3.BackColor = System.Drawing.Color.White;
            this.pbNb3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbNb3.Location = new System.Drawing.Point(18, 322);
            this.pbNb3.Name = "pbNb3";
            this.pbNb3.Size = new System.Drawing.Size(150, 150);
            this.pbNb3.TabIndex = 7;
            this.pbNb3.TabStop = false;
            this.pbNb3.Tag = "3";
            this.pbNb3.Click += new System.EventHandler(this.pbNb1_Click);
            // 
            // pbClose
            // 
            this.pbClose.BackColor = System.Drawing.Color.LightGray;
            this.pbClose.Location = new System.Drawing.Point(927, 9);
            this.pbClose.Name = "pbClose";
            this.pbClose.Size = new System.Drawing.Size(37, 23);
            this.pbClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbClose.TabIndex = 37;
            this.pbClose.TabStop = false;
            this.pbClose.Click += new System.EventHandler(this.pbClose_Click);
            this.pbClose.MouseEnter += new System.EventHandler(this.pbClose_MouseEnter);
            this.pbClose.MouseLeave += new System.EventHandler(this.pbClose_MouseLeave);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(976, 666);
            this.Controls.Add(this.pbClose);
            this.Controls.Add(this.pnlShow);
            this.Controls.Add(this.pnlChooser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Imprimantes";
            this.pnlChooser.ResumeLayout(false);
            this.pnlShow.ResumeLayout(false);
            this.pnlShow.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPrinter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNb5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNb4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNb2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbNb3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel pnlChooser;
        private System.Windows.Forms.PictureBox pbNb5;
        private System.Windows.Forms.PictureBox pbNb1;
        private System.Windows.Forms.PictureBox pbNb4;
        private System.Windows.Forms.PictureBox pbNb2;
        private System.Windows.Forms.PictureBox pbNb3;
        private System.Windows.Forms.Panel pnlShow;
        private System.Windows.Forms.PictureBox pbPrinter;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblBrand;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label _lblWeight;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label lblReleasePrice;
        private System.Windows.Forms.Label _lblReleasePrice;
        private System.Windows.Forms.Label _lblBuilder;
        private System.Windows.Forms.Label _lblPrinterSite;
        private System.Windows.Forms.Label lblPrintSpeed;
        private System.Windows.Forms.Label _lblPrintSpeed;
        private System.Windows.Forms.Label lblBuilder;
        private System.Windows.Forms.Label _lblProvider;
        private System.Windows.Forms.Label lblProvider;
        private System.Windows.Forms.Label lblPrinterSite;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblCartridgeBrand;
        private System.Windows.Forms.Label _lblCartridgeBrand;
        private System.Windows.Forms.Label lblCartridgeType;
        private System.Windows.Forms.Label lblCartridgePrice;
        private System.Windows.Forms.Label _lblCartridgePrice;
        private System.Windows.Forms.Label _lblSides;
        private System.Windows.Forms.Label _lblCartridgeType;
        private System.Windows.Forms.Label lblSides;
        private System.Windows.Forms.Label _lblDimension;
        private System.Windows.Forms.Label lblDimension;
        private System.Windows.Forms.Label _lblResolution;
        private System.Windows.Forms.Label lblResolution;
        private System.Windows.Forms.PictureBox pbClose;
    }
}

