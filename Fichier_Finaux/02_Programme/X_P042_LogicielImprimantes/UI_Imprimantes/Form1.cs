﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI_Imprimantes
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        static public string[,] printerArray = new string[16,30];
        static int x;

        private void Show(int _x)
        {
            lblModel.Text = printerArray[x, 0];
            lblDimension.Text = printerArray[x, 1];
            lblWeight.Text = printerArray[x, 2];
            lblPrice.Text = printerArray[x, 3];
            lblReleasePrice.Text = printerArray[x, 4];
            lblBrand.Text = printerArray[x, 5];
            lblBuilder.Text = printerArray[x, 6];
            lblProvider.Text = printerArray[x, 7];
            lblPrintSpeed.Text = printerArray[x, 8];
            lblResolution.Text = printerArray[x, 9];
            lblSides.Text = printerArray[x, 10];
            lblPrinterSite.Text = printerArray[x, 11];
            lblCartridgePrice.Text = printerArray[x, 12];
            lblCartridgeBrand.Text = printerArray[x, 13];
            lblCartridgeType.Text = printerArray[x, 14];
        }
        private void pbNb1_Click(object sender, EventArgs e)
        {
            #region fakedata
            printerArray[0, 0] = "Brother";
            printerArray[1, 0] = "Canon";
            #endregion          
            switch (((Control)sender).Tag)
            {
              
                case "1":
                    x = 0;
                    Show(x);
                    break;
                case "2":
                    x = 1;
                    Show(x);
                    break;
                case "3":
                    x = 2;
                    Show(x);
                    break;
                case "4":
                    x = 3;
                    Show(x);
                    break;
                case "5":
                    x = 4;
                    Show(x);
                    break;
                    
            }

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pbClose_MouseEnter(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = Color.IndianRed;
        }

        private void pbClose_MouseLeave(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = Color.LightGray;

        }

        private void pbClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }


}
