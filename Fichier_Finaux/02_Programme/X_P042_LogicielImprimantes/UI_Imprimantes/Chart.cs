﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace UI_Imprimantes
{
    public partial class Chart: Form
    {
        //Imprimantes correspondant 

        public int _x;
        public Chart()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Nous avons récuperer ces trois méthodes sur internet, ces dernière permettent de déplacer la form avec la souris
        /// </summary>
        private bool mouseDown;
        private Point lastLocation;
        private void FrmMain_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }
        private void FrmMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }
        private void FrmMain_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }


        /// <summary>
        /// Recolor le bouton pour quitter de la meme manière que dans la form principal
        /// </summary>
        private void pbClose_MouseEnter(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = Color.IndianRed;
        }
        private void pbClose_MouseLeave(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = Color.LightGray;

        }

        /// <summary>
        /// Ferme la chart et met à jour la variables bool correspondante
        /// </summary>
        private void pbClose_Click(object sender, EventArgs e)
        {
            this.Close();
            frmMain.IsChartOpen = false;
        }

        /// <summary>
        /// Lorsque la chart load, on met les informations des prix dans le graphique
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Chart_Load(object sender, EventArgs e)
        {
            chrtSellPrice.Series[1].Points.AddXY(0, frmMain.compiledData[2 + _x * 15]);
            chrtSellPrice.Series[0].Points.AddXY(0,frmMain.compiledData[3 + _x * 15]);
        }

        /// <summary>
        /// Focus sur le graphique si on passe la souris dessus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Chart_MouseEnter(object sender, EventArgs e)
        {
            this.Focus();
        }
    }
}
