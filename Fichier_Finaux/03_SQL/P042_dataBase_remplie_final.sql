-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 18 Décembre 2019 à 09:06
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `p_042_imprimantes`
--
create database P_042_Imprimantes;
use P_042_Imprimantes;
-- --------------------------------------------------------

--
-- Structure de la table `t_cartouche`
--

CREATE TABLE `t_cartouche` (
  `idCartouche` int(11) NOT NULL COMMENT 'Cle primaire de t_cartouche',
  `carNom` varchar(50) NOT NULL COMMENT 'Nom de la cartouche',
  `carType` varchar(50) NOT NULL COMMENT 'Type de la cartouche',
  `carPrix` decimal(10,2) NOT NULL COMMENT 'Prix de la catouche',
  `idMarque` int(11) NOT NULL COMMENT 'Cle etrangere de la table t_marque'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_cartouche`
--

INSERT INTO `t_cartouche` (`idCartouche`, `carNom`, `carType`, `carPrix`, `idMarque`) VALUES
(1, 'Brother TN-423BK', 'Toner', '101.00', 1),
(2, 'Brother TN-2420', 'Toner', '71.30', 1),
(3, 'Brother LC-3213BK', 'Encre', '22.95', 1),
(4, 'Brother TN-2320', 'Toner', '11.90', 1),
(5, 'Brother TN-2320', 'Toner', '14.90', 1),
(6, 'Brother TN-243', 'Toner', '47.70', 1),
(7, 'Brother TN-2420', 'Toner', '71.30', 1),
(8, 'Brother LC-3219XLBK', 'Encre', '39.00', 1),
(9, 'Brother TN-2420', 'Toner', '119.95', 1),
(10, 'Brother TN-2420', 'Toner', '119.96', 1),
(11, 'Canon PGI-580XL', 'Encre', '19.95', 2),
(12, 'Canon CLI-581XL', 'Encre', '15.00', 2),
(13, 'Canon PGI-580XL', 'Encre', '19.95', 2),
(14, 'Canon PGI-580XL', 'Encre', '19.95', 2),
(15, 'Canon CLI-581BK XL', 'Encre', '18.95', 2),
(16, 'Canon PG-540XL', 'Encre', '34.95', 2),
(17, 'Canon 054 H BK BK', 'Toner', '85.00', 2),
(18, 'Epson 24XL 0001304475', 'Encre', '16.95', 3),
(19, 'Epson EcoTank C13T00P140', 'Encre', '10.90', 4),
(20, 'Epson 27XL DURABrite C13T27114012', 'Encre', '38.90', 4),
(21, 'EcoTank C13T00P140', 'Encre', '10.90', 4),
(22, 'Epson 34XL noir T3471', 'Encre', '9.10', 4),
(23, 'HP 304', 'Encre', '15.00', 4),
(24, 'HP 304', 'Encre', '15.00', 4),
(25, 'HP 31 (CF, encre)', 'Encre', '33.30', 4),
(26, 'HP 963XL', 'Encre', '53.00', 4),
(27, 'HP 303', 'Encre', '20.00', 4),
(28, 'HP 302', 'Encre', '23.00', 4),
(29, 'Xerox 106R03480', 'Toner', '126.00', 5),
(30, 'Xerox 106R03480', 'Toner', '126.00', 5);

-- --------------------------------------------------------

--
-- Structure de la table `t_constructeur`
--

CREATE TABLE `t_constructeur` (
  `idConstructeur` int(11) NOT NULL COMMENT 'Cle primaire de t_constructeur',
  `conNom` varchar(50) NOT NULL COMMENT 'Nom du constructeur'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_constructeur`
--

INSERT INTO `t_constructeur` (`idConstructeur`, `conNom`) VALUES
(1, 'Brother'),
(2, 'Canon'),
(3, 'Epson'),
(4, 'HP'),
(5, 'Xerox');

-- --------------------------------------------------------

--
-- Structure de la table `t_imprimante`
--

CREATE TABLE `t_imprimante` (
  `idImprimante` int(11) NOT NULL COMMENT 'Cle primaire de t_imprimante',
  `impModele` varchar(50) NOT NULL COMMENT 'Modele de l`imprimante',
  `impPrixActuel` decimal(10,2) NOT NULL COMMENT 'Prix Actuel de l`imprimante',
  `impPrixSortie` decimal(10,2) NOT NULL COMMENT 'Prix de sortie de l`imprimante',
  `impPoids` varchar(50) NOT NULL COMMENT 'Poids de l`imprimante',
  `impLargeur` varchar(50) NOT NULL COMMENT 'Largeur de l`imprimante',
  `impLongueur` varchar(50) NOT NULL COMMENT 'Longueur de l`imprimante',
  `impHauteur` varchar(50) NOT NULL COMMENT 'Hauteur de l`imprimante',
  `impVitesseImpression` varchar(50) NOT NULL COMMENT 'Vitesse d`impression de l`imprimante',
  `impResolution` varchar(50) NOT NULL COMMENT 'Resolution de l`imprimante',
  `impRectoVerso` varchar(50) NOT NULL COMMENT 'Vrai ou Faux si l`imprimante fait du RectoVerso',
  `impSite` varchar(50) NOT NULL COMMENT 'Site de l`imprimante',
  `idMarque` int(11) NOT NULL COMMENT 'Cle etrangere de la table t_marque',
  `idCartouche` int(11) NOT NULL COMMENT 'Cle etrangere de la table t_cartouche',
  `idConstructeur` int(11) NOT NULL COMMENT 'Cle etrangere de la table t_constructeur'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_imprimante`
--

INSERT INTO `t_imprimante` (`idImprimante`, `impModele`, `impPrixActuel`, `impPrixSortie`, `impPoids`, `impLargeur`, `impLongueur`, `impHauteur`, `impVitesseImpression`, `impResolution`, `impRectoVerso`, `impSite`, `idMarque`, `idCartouche`, `idConstructeur`) VALUES
(1, 'Brother MFC-L8690CDW', '377.99', '640.48', '27.9', '52.6', '43.5', '53.9', '31 ppm', '1200 x 600 dpi', 'Automatique', 'Digitec.ch', 1, 1, 1),
(2, 'Brother DCP-L2530DW', '152.85', '155.05', '10.3', '39.9', '41', '27.2', '30 ppm', '600 x 2400 dpi', 'Manuel', 'Digitec.ch', 1, 2, 1),
(3, 'Brother DCP-J774DW', '99.00', '122.00', '6.6', '40', '34.1', '15.1', '27 ppm', '6000 x 1200 dpi', 'Automatique', 'Microspot.ch', 1, 3, 1),
(4, 'Brother MFC-J1300DW', '322.95', '379.90', '8', '34.1', '43.5', '19.5', '12 ppm', '2400 x 1200 dpi', 'Automatique', 'Conrad.ch', 1, 4, 1),
(5, 'Brother MFC-L3710CW', '423.00', '482.00', '22.5', '41', '47.5', '41.4', '18 ppm', '600 x 600dpi', 'Automatique', 'Microspot.ch', 1, 5, 1),
(6, 'Brother MFC-L3770CDW', '365.45', '440.90', '24.5', '41', '47.5', '41.1', '25 ppm', '2400 x 600 dpi', 'Automatique', 'Microspot.ch', 1, 6, 1),
(7, 'Brother MFC-L2710DW', '152.95', '151.25', '11.8', '39.8', '41', '31.8', '30 ppm', '1200 x 1200 dpi', 'Manuel', 'Digitec.ch', 1, 7, 1),
(8, 'Brother MFC-J6930DW', '271.15', '354.00', '23.6', '49', '57', '67.5', '35 ppm', '1200 x 2400 dpi', 'Manuel', 'Digitec.ch', 1, 8, 1),
(9, 'Brother MFC-L2750DW', '246.45', '305.80', '11.8', '41', '39.9', '31.9', '34 ppm', '600 x 600 dpi', 'Manuel', 'Mediamarkt.ch', 1, 9, 1),
(10, 'Brother MFC-L2730DW', '212.45', '243.60', '10.9', '41', '39.8', '31.8', '34 ppm', '2400 x 600 dpi', 'Automatique', 'Mediamarkt.ch', 1, 10, 1),
(11, 'Canon Pixma TS6251', '72.95', '79.90', '6.2', '31.5', '13.9', '37.2', '15 ppm', '1200 x 2400 dpi', 'Automatique', 'Fust.ch', 2, 11, 2),
(12, 'Canon Pixma TS8150', '95.00', '146.40', '6.4', '32.4', '13.9', '37.2', '15 ppm', '2400 x 4800 dpi', 'Automatique', 'Digitec.ch', 2, 12, 2),
(13, 'Canon Pixma TS9550', '181.00', '227.00', '9.7', '36.6', '19.3', '46.8', '15 ppm', '4800 x 1200 dpi', 'Automatique', 'Fust.ch', 2, 13, 2),
(14, 'Canon Pixma G4510', '310.95', '245.00', '5.7', '44.9', '30.4', '15.2', '3.5 ppm', '4800 x 1200 dpi', 'Manuel', 'Interdiscount.ch', 2, 14, 2),
(15, 'Canon Pixma TS9155', '200.55', '222.00', '6.7', '37.2', '32.4', '14', '15 ppm', '4800 x 1200 dpi', 'Manuel', 'Fust.ch', 2, 15, 2),
(16, 'Canon Pixma MG3650S', '61.15', '56.90', '5.4', '44.9', '15.2', '30.4', '9.9 ppm', '1200 x 2400 dpi', 'Automatique', 'Interdiscount.ch', 2, 16, 2),
(17, 'Canon MF643Cdw i-SENSYS', '236.95', '268.00', '22.6', '45.1', '46', '41.3', '21 ppm', '600 x 600 dpi', 'Automatique', 'Digitec.ch', 2, 17, 2),
(18, 'Expression Premium XP-6105', '83.95', '107.90', '6.6', '34', '14.2', '34.9', '15.8 ppm', '1.200 x 4.800', 'Automatique', 'Fust.ch', 3, 18, 3),
(19, 'EPSON EcoTank ET-2720', '203.00', '236.00', '4', '37.5', '34.7', '17.9', '10 ppm', '1.200 x 2.400 dpi', 'Manuel', 'Interdiscount.ch', 3, 19, 3),
(20, 'EPSON WorkForce WF-7710DWF', '161.00', '189.90', '18.5', '48.6', '34', '56.7', '32 ppm', '4800 x 2400 dpi', 'Automatique', 'conforama.ch', 3, 20, 3),
(21, 'Epson EcoTank ET-2726', '212.40', '307.00', '4', '37.5', '34.7', '17.9', '5 ppm', '5760 x 1440 dpi', 'Manuel', 'Fust.ch', 3, 21, 3),
(22, 'WorkForce WF-3720DWF', '93.40', '152.65', '8.6', '37.8', '24.9', '42.5', '20 ppm', '1.200 x 2.400 dpi', 'Automatique', 'Interdiscount.ch', 3, 22, 3),
(23, 'HP Deskjet 2620', '40.66', '39.95', '3.4', '42.5', '30.4', '14.9', '20 ppm', '600 x 300 dpi', 'Manuel', 'Fust.ch', 4, 23, 4),
(24, 'HP DeskJet 3750', '51.50', '68.14', '2.3', '45', '20', '15', '8 ppm', '1200 x 1200 dpi', 'Manuel', 'Fust.ch', 4, 24, 4),
(25, 'HP Smart Tank Plus 555', '299.00', '351.78', '5.1', '44.7', '37.3', '25.9', '22 ppm', '1200 x 1200 dpi', 'Manuel', 'Fust.ch', 4, 25, 4),
(26, 'HP OfficeJet Pro 9019 AiO', '253.95', '280.11', '8.9', '43.9', '34.2', '27.8', '22 ppm', '4800 x 1200 dpi', 'Automatique', 'interdiscount.ch', 4, 26, 4),
(27, 'HP ENVY Photo 7830', '119.90', '134.90', '7.6', '45.4', '19.3', '49.1', '22 ppm', '1200 x 1200 dpi', 'Automatique', 'Microspot.ch', 4, 27, 4),
(28, 'HP Officejet 5232', '99.80', '79.90', '6.5', '44.5', '36.9', '19', '10 ppm', '1200 x 1200 dpi', 'Manuel', 'Fust.ch', 4, 28, 4),
(29, 'Xerox VersaLink C500DN', '400.05', '479.90', '27.6', '45.7', '40.6', '17.5', '43 ppm', '1200 x 2400 dpi', 'Automatique', 'steg-electronics.ch', 5, 29, 5),
(30, 'Xerox Phaser 6510/N', '164.35', '152.90', '23.8', '48.3', '42.2', '34.7', '28 ppm', '1200 x 2400 dpi', 'Manuel', 'steg-electronics.ch', 5, 30, 5);

-- --------------------------------------------------------

--
-- Structure de la table `t_marque`
--

CREATE TABLE `t_marque` (
  `idMarque` int(11) NOT NULL COMMENT 'Cle primaire de t_marque',
  `marNom` varchar(50) NOT NULL COMMENT 'Nom de la marque'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_marque`
--

INSERT INTO `t_marque` (`idMarque`, `marNom`) VALUES
(1, 'Brother'),
(2, 'Canon'),
(3, 'Epson'),
(4, 'HP'),
(5, 'Xerox');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_cartouche`
--
ALTER TABLE `t_cartouche`
  ADD PRIMARY KEY (`idCartouche`),
  ADD UNIQUE KEY `ID_t_cartouche_IND` (`idCartouche`),
  ADD KEY `FKavoir3_IND` (`idMarque`);

--
-- Index pour la table `t_constructeur`
--
ALTER TABLE `t_constructeur`
  ADD PRIMARY KEY (`idConstructeur`),
  ADD UNIQUE KEY `ID_t_constructeur_IND` (`idConstructeur`);

--
-- Index pour la table `t_imprimante`
--
ALTER TABLE `t_imprimante`
  ADD PRIMARY KEY (`idImprimante`),
  ADD UNIQUE KEY `ID_t_imprimante_IND` (`idImprimante`),
  ADD KEY `FKavoir2_IND` (`idMarque`),
  ADD KEY `FKutiliser_IND` (`idCartouche`),
  ADD KEY `FKavoir_IND` (`idConstructeur`);

--
-- Index pour la table `t_marque`
--
ALTER TABLE `t_marque`
  ADD PRIMARY KEY (`idMarque`),
  ADD UNIQUE KEY `ID_t_marque_IND` (`idMarque`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_cartouche`
--
ALTER TABLE `t_cartouche`
  MODIFY `idCartouche` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Cle primaire de t_cartouche', AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `t_constructeur`
--
ALTER TABLE `t_constructeur`
  MODIFY `idConstructeur` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Cle primaire de t_constructeur', AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `t_imprimante`
--
ALTER TABLE `t_imprimante`
  MODIFY `idImprimante` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Cle primaire de t_imprimante', AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `t_marque`
--
ALTER TABLE `t_marque`
  MODIFY `idMarque` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Cle primaire de t_marque', AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_cartouche`
--
ALTER TABLE `t_cartouche`
  ADD CONSTRAINT `FKavoir3_FK` FOREIGN KEY (`idMarque`) REFERENCES `t_marque` (`idMarque`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `t_imprimante`
--
ALTER TABLE `t_imprimante`
  ADD CONSTRAINT `FKavoir_FK` FOREIGN KEY (`idConstructeur`) REFERENCES `t_constructeur` (`idConstructeur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKutiliser_FK` FOREIGN KEY (`idCartouche`) REFERENCES `t_cartouche` (`idCartouche`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_imprimante_ibfk_1` FOREIGN KEY (`idMarque`) REFERENCES `t_marque` (`idMarque`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
